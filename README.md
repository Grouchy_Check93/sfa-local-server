## Fingerprint Authentication System (Local proxy server)

### Edit environmental variables:

Copy `.env.sample` to `.env` and edit the contents of the environment file to your required configuration setup.

### Development Setup:

```
git clone git@gitlab.com:computer-security1/finger-verify.git
cd finger-verify
virtualenv --python=$(which python3) venv
source venv/bin/activate
pip install -r requirements.txt
```

### Generate pub-priv key pair:

```
python3 -m src.keygen
```

### Initialize database:

```
python3 -m db.init
```

### Run the code

```
python3 -m run
```