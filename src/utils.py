""" Utility functions """

import hashlib
import math


def encode_regno(regno):
    ascii_regno_list = [f"{ord(i):02d}" for i in regno.upper()]
    ascii_regno = int("".join(ascii_regno_list))

    return ascii_regno


def decode_regno(ascii_regno_undiv):
    ascii_regno_list_undiv = [i for i in str(ascii_regno_undiv)]

    ascii_regno_list = []
    for i in range(0, len(ascii_regno_list_undiv), 2):
        ascii_regno_list.append(int("".join(ascii_regno_list_undiv[i : i + 2])))
        regno = "".join(chr(ascii_regno) for ascii_regno in ascii_regno_list)

    return regno


def sha256(plaintext):
    return hashlib.sha256(plaintext.encode()).hexdigest()


def cal_euler_distance(result1, result2):
    distance = 0
    for i in range(len(result1)):
        distance += math.pow(result1[i] - result2[i], 2)
    return distance
