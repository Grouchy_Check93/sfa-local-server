from .paillier import Paillier

if __name__ == "__main__":
    paillier = Paillier(256)
    paillier.generate_key_pair()
