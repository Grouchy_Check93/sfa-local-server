from sqlite3 import Error


def create_table(conn):
    """
    Create attendance table
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    sql_create_attendance_table = """ CREATE TABLE IF NOT EXISTS attendance (
                                        id integer PRIMARY KEY,
                                        regno text NOT NULL,
                                        status boolean NOT NULL,
                                        dt datetime default current_timestamp
                                    ); """

    try:
        c = conn.cursor()
        c.execute(sql_create_attendance_table)
    except Error as e:
        print(e)


def mark_attendance(conn, attendance):
    """
    Mark attendance entry
    :param conn:
    :param attendance:
    :return:
    """

    sql = """ INSERT INTO attendance(regno, dt, status)
              VALUES(?,?,?) """
    cur = conn.cursor()
    cur.execute(sql, attendance)
    conn.commit()
