import cv2
import numpy as np
import matplotlib.pyplot as plt
import math

from .sector import cal_mean, divide_sector, normalize_img
from .gabor import get_gabor


class FingerCode:
    def __init__(self, image):
        self.result = self.extract_fingercode(image)

    def get_central_point(self, img):
        img1 = img.copy()
        img = cv2.GaussianBlur(img, (3, 3), 0)
        img = cv2.blur(img, (5, 5))
        sobelx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=3)

        sobelx1 = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=3)

        A = sobelx.copy()
        B = sobelx1 + A
        gh = cv2.Sobel(B, cv2.CV_16S, 1, 1, ksize=3)
        gh_blur = cv2.GaussianBlur(gh, (15, 15), 0)
        gh_blur = cv2.convertScaleAbs(gh_blur, gh_blur)
        gh_media = cv2.medianBlur(gh_blur, 5)
        gh_media = cv2.medianBlur(gh_media, 5)
        gh_media = cv2.medianBlur(gh_media, 3)
        rows, cols = img.shape[:2]

        gh_media = cv2.convertScaleAbs(gh_media)
        gh_media = cv2.cvtColor(gh_media, cv2.COLOR_BGR2GRAY)
        ret1, binary1 = cv2.threshold(gh_media, 35, 255, cv2.THRESH_BINARY)

        kernel = np.ones((5, 5), np.uint8)
        gh_blur = cv2.erode(binary1, kernel, iterations=1)
        rows, cols = gh_blur.shape[:2]
        max = 0.0
        row = 0
        col = 0
        for i in range(rows):
            for j in range(cols):
                if binary1[i][j] > 0:
                    max = gh_blur[i][j]
                    row = i
                    col = j

        rows, cols = img.shape[:2]
        if row != 0:
            if (col + 75 < cols) & (row + 75 < rows):
                print("Read success")
            else:
                row = 0
                col = 0
                print(" Read failed")
        else:
            print("failed")

        return col, row

    def get_core_img(self, img, core_x, core_y):
        radius = 75
        core_img = img[
            core_y - radius : core_y + radius, core_x - radius : core_x + radius
        ]
        return core_img

    def cal_standar(self, points, mean):
        total = 0
        for i in range(len(points)):
            point = points[i]
            total += math.pow(point.Gray - mean, 2)
        if len(points) != 0:
            return total / len(points)
        else:
            return 0

    def fingercode(self, img):
        sectors = divide_sector(img)
        result = []
        Mean = []
        for i in range(len(sectors)):
            Mean.append(cal_mean(sectors[i]))

        Variance = []
        for i in range(len(sectors)):
            Variance.append(self.cal_standar(sectors[i], Mean[i]))

        for i in range(len(sectors)):
            temp = round(math.sqrt(Variance[i]), 0)
            # print temp
            result.append(int(temp))
        return result

    def extract_fingercode(self, image):
        img = cv2.imread(image)
        rows, cols = img.shape[:2]
        # get the reference point
        core_x, core_y = self.get_central_point(img)

        if core_x != 0:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            core_img = self.get_core_img(img, core_x, core_y)

            # divide the img sector
            sectors = divide_sector(core_img)

            core_img = normalize_img(core_img, sectors)

            result = get_gabor(core_img)  # 8

            ADD = []
            for i in result:
                fingercodetemp = self.fingercode(i)
                ADD.extend(fingercodetemp)

            return ADD
