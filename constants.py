import os

from dotenv import load_dotenv
from pathlib import Path  # Python 3.6+ only

env_path = Path(".") / ".env"
load_dotenv(dotenv_path=env_path)

REMOTE_SRV_URI = os.getenv("REMOTE_SRV_URI")
ENROLL_URI = REMOTE_SRV_URI + "/enroll"
VALIDATE_URI = REMOTE_SRV_URI + "/validate"
