import string
import random
import json
import requests
from datetime import datetime

from src.paillier import Paillier
from src.utils import (
    encode_regno,
    decode_regno,
    sha256,
)
from constants import ENROLL_URI, VALIDATE_URI
from fingercode.fingercode import FingerCode
from db.queries import mark_attendance
from db.conn import create_connection

# FP_DATA_LEN = 200
# random_fp = "".join(
#     random.choices(string.ascii_lowercase + string.digits, k=FP_DATA_LEN)
# )
# random_fp = "wct4rywzkxdpca1ktds46i8izwgneauujtqk9o2jhlz101pcbr5935vgnw1hfuby84hcfnln9srso2nrke1farydlkbgda1nesf7q0jpj62ugxqffw5knfapchcrhgodi8dk4415vwuw4euacgs3ypp1ogb3a9es6meu5vfupjrfd92yvgn0sbuqxxs4omaw0s3xcxws"
# print(random_fp, len(random_fp))
# random_fp_ascii = [ord(ele) for ele in random_fp]

# print(random_fp_ascii)

# print(random_fp)

paillier = Paillier(256)
paillier.load_keys()

RANDOM_REGNO = "B170005CS"
DB_FILE = "./dumps/local.db"
THRESHOLD = 500


def enroll(random_fp_ascii):

    encr_fp_list = []
    for i in range(len(random_fp_ascii)):
        encr_obj = paillier.encrypt(random_fp_ascii[i])
        encr_fp_list.append(paillier.serialize(encr_obj))

    encr_fp_sqr = paillier.serialize(paillier.encr_sqr_sum(random_fp_ascii))

    # encr_regno = paillier.serialize(paillier.encrypt(encode_regno(RANDOM_REGNO)))

    # print(encr_regno)

    data = {
        "encr_regno": sha256(RANDOM_REGNO),
        "encr_fp_list": encr_fp_list,
        "encr_fp_sqr": encr_fp_sqr,
    }

    print(data["encr_regno"])

    fp = open("sample.json", "w")
    json.dump(data, indent=4, fp=fp)

    s = requests.Session()
    resp = s.post(
        ENROLL_URI,
        data=json.dumps(data),
        headers={"content-type": "application/json"},
    )
    print(resp)


def validate(conn, new_fp):
    data = {"encr_regno": sha256(RANDOM_REGNO)}

    s = requests.Session()
    resp = s.post(
        VALIDATE_URI,
        data=json.dumps(data),
        headers={"content-type": "application/json"},
    )
    resp_json = json.loads(resp.text)

    encr_fp_orig = []

    encr_fp_sqr = paillier.deserialize(json.loads(resp_json["encr_fp_sqr"]))

    for i in range(len(resp_json["encr_fp_list"])):
        encr_fp_orig.append(
            paillier.deserialize(json.loads(resp_json["encr_fp_list"][i]))
        )

    # print(encr_fp_orig)
    # print(encr_fp_sqr)

    d = paillier.get_alt_euclidean_dist(encr_fp_orig, encr_fp_sqr, new_fp)
    print("Euclidean distance : ", d)
    if d < THRESHOLD:
        attendance = (RANDOM_REGNO, datetime.now(), 1)
        mark_attendance(conn, attendance)
    else:
        print("Threshold too high!")


if __name__ == "__main__":

    conn = create_connection(DB_FILE)

    if conn is None:
        print("Error! cannot create the database connection.")

    if paillier.decrypt(paillier.encrypt(10)) == 10:
        x = paillier.serialize(paillier.encrypt(10))
        y = paillier.deserialize(json.loads(x))
        print("Paillier working!")

    print("Choose option : \n1. Enroll\n2. Validate\n")
    opt = int(input())

    if opt == 1:
        fp_img_path = "./dataset/101_1.tif"
        fp_arr = FingerCode(fp_img_path).result
        enroll(fp_arr)
    elif opt == 2:
        fp_img_new = "./dataset/101_2.tif"
        fp_arr_new = FingerCode(fp_img_new).result
        validate(conn, fp_arr_new)